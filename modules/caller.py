'''
Module 'callers' - Contains classes and functions for calling variants from next generation sequencing data
'''

## DEPENDENCIES ##
# External
import sys
import multiprocessing as mp
import os
import pysam
import time
import subprocess
import itertools

# GAPI
from GAPI import log
from GAPI import unix
from GAPI import databases
from GAPI import formats
from GAPI import bamtools
from GAPI import structures
from GAPI import events
from GAPI import clusters
from GAPI import output
from GAPI import annotation
from GAPI import filters
from GAPI import gRanges

# Internal
from modules import bkp
from modules import retrotransposons
from modules import output


## FUNCTIONS ##

## CLASSES ##
class transduction_caller():
    '''
    Tranduction caller for Illumina sureselect data targetering source element´s downstream regions
    '''
    def __init__(self, mode, bam, normalBam, reference, refDir, confDict, outDir):

        self.mode = mode
        self.bam = bam
        self.normalBam = normalBam
        self.reference = reference
        self.refDir = refDir
        self.confDict = confDict
        self.outDir = outDir
        self.repeatsBinDb = None

        ## Compute reference lengths
        self.refLengths = bamtools.get_ref_lengths(self.bam)

    def call(self):
        '''
        Search for structural variants (SV) in a set of target genomic regions
        '''

        ### 1. Infer read size
        self.confDict['readSize'] = bamtools.infer_readSize(self.bam)
        
        ### 2. Create bed file containing source element´s transduced intervals 
        tdDir = self.outDir + '/TRANSDUCED/'
        unix.mkdir(tdDir)
        sourceBed = self.confDict['srcBed'] if self.confDict['srcBed'] != None else self.refDir + '/srcElements.bed'
        transducedPath = databases.create_transduced_bed(sourceBed, self.confDict['tdEnd'], 10000, -int(self.confDict['readSize']/4), tdDir)

        ### 3. Load annotations if running method on wgs data
        if self.confDict['retroTestWGS']:
            annotDir = self.outDir + '/ANNOT/'
            # TO DO: if running for detecting SVA TDs --> this DB should be changed. Create 'REPEATS-SVA'
            self.annotations = annotation.load_annotations(['REPEATS-L1', 'TRANSDUCTIONS'], self.refLengths, self.refDir, self.confDict['germlineMEI'], self.confDict['processes'], annotDir, tdEnd = self.confDict['tdEnd'])
            unix.rm([annotDir])
            
        ### 3. Define genomic bins to search for SV (will correspond to transduced areas)
        bins = bamtools.binning(transducedPath, None, None, None)

        ## Organize bins into a dictionary
        self.confDict['rangesDict'] = gRanges.rangeList2dict(bins)

        ### 4. Associate to each bin the src identifier
        BED = formats.BED()
        BED.read(transducedPath, 'List', None)   

        for index, coords in enumerate(bins):
            coords.append(BED.lines[index].optional['cytobandId'])
        
        ### 5. Search for SV clusters in each bin 
        # Genomic bins will be distributed into X processes
        pool = mp.Pool(processes=self.confDict['processes'])
        clusterPerSrc = pool.starmap(self.make_clusters_bin, bins)
        pool.close()
        pool.join()
        
        # Convert into dictionary
        clusterPerSrcDict = {srcId:clusters for srcId,clusters in clusterPerSrc}

        ### 6. Annotate metaclusters
        # Create output directory
        annotDir = self.outDir + '/ANNOT/'
        unix.mkdir(annotDir)
        if self.confDict['annovarDir'] is not None:
            metaclusters = [cluster for srcId, clusters in clusterPerSrc for cluster in clusters]
            annotation.annotate(metaclusters, ['GENE'], None, self.confDict['annovarDir'], annotDir)
        
        ### 7. Write calls to file
        output.write_transduction_calls(clusterPerSrcDict, self.outDir, self.confDict['germlineMEI'])
        
        ### 8. Clean up
        unix.rm([tdDir])
        unix.rm([annotDir])
        unix.rm([self.outDir + '/SUPPLEMENTARY'])
        unix.rm([self.outDir + '/BKP'])
            

    def make_clusters_bin(self, ref, beg, end, srcId):
        '''
        Search for structural variant (SV) clusters in a genomic bin/window
        '''
        ## 0. Set bin id and create bin directory ##
        binId = '_'.join([str(ref), str(beg), str(end)])
        msg = 'SV calling in bin: ' + binId
        log.subHeader(msg)

        ## 1. Search for discordant and clipped read events in the bam file/s ##
        # a) Single sample mode
        if self.mode == "SINGLE":
            eventsDict = bamtools.collectSV(ref, beg, end, self.bam, self.confDict, None)

        # b) Paired sample mode (tumour & matched normal)
        else:
            eventsDict = bamtools.collectSV_paired(ref, beg, end, self.bam, self.normalBam, self.confDict)

        step = 'COLLECT'
        SV_types = sorted(eventsDict.keys())
        counts = [str(len(eventsDict[SV_type])) for SV_type in SV_types]
        msg = 'Number of SV events in bin (' + ','.join(['binId'] + SV_types) + '): ' + '\t'.join([binId] + counts)
        log.step(step, msg)

        ## If search for supplementary alignments selected:
        if self.confDict['blatClip']:
            
            ## 2. Search for supplementary alignments by realigning the clipped sequences
            step = 'SEARCH4SUPPL'
            msg = 'Search for supplementary alignments by realigning the clipped sequences'
            log.step(step, msg)

            ## Create output directory 
            supplDir = self.outDir + '/SUPPLEMENTARY/' + srcId
            unix.mkdir(supplDir)
            
            ## Left-clippings
            events.search4supplementary(eventsDict['LEFT-CLIPPING'], self.reference, srcId, supplDir)
                
            ## Rigth-clippings
            events.search4supplementary(eventsDict['RIGHT-CLIPPING'], self.reference, srcId, supplDir)

            ## Remove output directory
            unix.rm([supplDir])

        ## 3. Discordant and clipping clustering ##
        ## 3.1 Organize discordant and clipping events into genomic bins prior clustering ##
        step = 'BINNING'
        msg = 'Organize discordant and clipping events into genomic bins prior clustering'
        log.step(step, msg)
        
        ## Create bin database with discordants
        discordantsDict = {}
        discordantsDict['DISCORDANT'] = eventsDict['DISCORDANT']

        binSizes = [1000, 10000, 100000, 1000000]  
        discordantsBinDb = structures.create_bin_database_interval(ref, beg, end, discordantsDict, binSizes)

        ## Create bin database with clippings 
        clippingsDict = {}
        clippingsDict['LEFT-CLIPPING'] = eventsDict['LEFT-CLIPPING']
        clippingsDict['RIGHT-CLIPPING'] = eventsDict['RIGHT-CLIPPING']

        binSizes = [self.confDict['maxBkpDist'], 100, 1000, 10000, 100000, 1000000]
        clippingsBinDb = structures.create_bin_database_interval(ref, beg, end, clippingsDict, binSizes)

        ## 3.2 Group discordant and clipping events into clusters ##
        step = 'CLUSTERING'
        msg = 'Group discordant and clipping events into clusters'
        log.step(step, msg)
 
        ## Discordant clustering
        discordantClustersBinDb = clusters.create_clusters(discordantsBinDb, self.confDict)

        ## Clipping clustering
        clippingClustersBinDb = clusters.create_clusters(clippingsBinDb, self.confDict)

        ## 3.3 Group discordant read pairs based on mate position ##
        step = 'GROUP-BY-MATE'
        msg = 'Group discordant read pairs based on mate position'
        log.step(step, msg)

        ## Make groups
        discordants = clusters.cluster_by_matePos(discordantClustersBinDb.collect(['DISCORDANT']), self.refLengths, self.confDict['minClusterSize'])

        ## 3.4 Group clipping events based on suppl alignment position ##
        step = 'GROUP-BY-SUPPL'
        msg = 'Group clipping events based on suppl alignment position'
        log.step(step, msg)

        ## Left clipping
        leftClippingClusters = clusters.cluster_by_supplPos(clippingClustersBinDb.collect(['LEFT-CLIPPING']), self.refLengths, self.confDict['minClusterSize'], 'LEFT-CLIPPING')

        ## Right clipping
        rightClippingClusters = clusters.cluster_by_supplPos(clippingClustersBinDb.collect(['RIGHT-CLIPPING']), self.refLengths, self.confDict['minClusterSize'], 'RIGHT-CLIPPING')

        ## 4. Cluster filtering ##
        ## 4.1 Discordant cluster filtering ##
        step = 'FILTER-DISCORDANT'
        msg = 'Discordant cluster filtering'
        log.step(step, msg)

        filters2Apply = ['MIN-NBREADS', 'MATE-REF', 'MATE-SRC', 'MATE-MAPQ', 'UNSPECIFIC', 'READ-DUP', 'CLUSTER-RANGE']
        
        if self.confDict['retroTestWGS']:
            filters2Apply.remove('UNSPECIFIC')
        
        filteredDiscordants = filters.filter_clusters(discordants, filters2Apply, self.bam, self.normalBam, self.confDict, 'DISCORDANT')

        ## 4.2 Clipping cluster filtering ##
        step = 'FILTER-CLIPPING'
        msg = 'Clipping cluster filtering'
        log.step(step, msg)
        
        filters2Apply = ['MIN-NBREADS', 'SUPPL-REF', 'SUPPL-SRC', 'SUPPL-MAPQ', 'UNSPECIFIC', 'READ-DUP', 'CLUSTER-RANGE']
        filteredLeftClippings = filters.filter_clusters(leftClippingClusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'CLIPPING')
        filteredRightClippings = filters.filter_clusters(rightClippingClusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'CLIPPING')
                    
        ## 5. Create metaclusters ##
        step = 'META-CLUSTERING'
        msg = 'Group discordant mates and suplementary clusters into metaclusters'
        log.step(step, msg)
        metaclusters = clusters.metacluster_mate_suppl(filteredDiscordants, filteredLeftClippings, filteredRightClippings, self.confDict['minReads'], self.refLengths)
        
        ## 6. Determine metaclusters precise coordinates ##
        step = 'DETERMINE-BKP'
        msg = 'Determine metaclusters breakpoints'
        log.step(step, msg)
        bkp.infer_bkp(metaclusters, self.bam, self.confDict['readSize'])
        
        ## 7. Determine metaclusters identity ##
        step = 'DEFINE-TD-TYPE'
        msg = 'Define metaclusters transduction type'
        log.step(step, msg)

        if self.confDict['retroTestWGS']:
            retrotransposons.identity_metaclusters_wgs(metaclusters, self.bam, self.outDir, self.confDict, self.annotations, srcId)
        else:
            retrotransposons.identity_metaclusters(metaclusters, self.bam, self.outDir, srcId)
        
        ## 8. Filter metaclusters ##
        step = 'FILTER-METACLUSTERS'
        msg = 'Filter metaclusters'
        log.step(step, msg)
        
        filters2Apply = ['MIN-NBREADS', 'AREAMAPQ', 'AREASMS', 'IDENTITY', 'GERMLINE', 'srcREF-TDs-ref6']
        
        if self.mode == 'SINGLE':
            filters2Apply.remove('GERMLINE')

        filteredMetaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        return [srcId, filteredMetaclusters]
    
    