'''
Module 'retrotransposons' - Contains functions for the identification and characterization of retrotransposon sequences
'''

## DEPENDENCIES ##
# External


# GAPI
from GAPI import log
from GAPI import unix
from GAPI import formats
from GAPI import alignment
from GAPI import sequences
from GAPI import annotation
from GAPI import bamtools
from GAPI import bkp
from GAPI import events
from GAPI import retrotransposons

## FUNCTIONS ##
def identity_metaclusters(metaclusters, bam, outDir, srcId):
    '''
    Determine metaclusters identity. If there is only a cluster and it contains a polyA tail, 
    it will be clasified as a partnered event. Else, it will be an orphan transduction. 
    
    Partnered:
    --------->
             ----AAAAAA>
       --------->
           ------AAAA>

    Orphan:
    --------->
             ----ACGTCA>
       --------->
           ------ACG>
    
    Input:
    1. metaclusters: List of metaclusters
    2. bam: Bam file
    3. outDir: output directory
    4. srcId: metacluster source id

    
    Output:
    Fill metacluster identity attribute with 'partnered' or 'orphan'    
    '''
    
    # set new confDict parameters to search for clippings
    newconfDict = {}
    newconfDict['targetEvents'] = ['CLIPPING']
    newconfDict['minMAPQ'] = 30
    newconfDict['minCLIPPINGlen'] = 8
    newconfDict['overhang'] = 0
    newconfDict['filterDuplicates'] = True
    newconfDict['readFilters'] = ['mateUnmap', 'insertSize', 'SMS']

    # for each metacluster
    for metacluster in metaclusters:
        
        # set src id
        metacluster.src_id = srcId
        
        # if there is no reciprocal clusters
        if metacluster.orientation != 'RECIPROCAL':

            ## 1. Collect clippings in region
            eventsDict = bamtools.collectSV(metacluster.ref, metacluster.refLeftBkp-100, metacluster.refRightBkp+100, bam, newconfDict, None, supplementary = False)
            
            ## 2. Create clipping consensus
            # create bkp dir
            bkpDir = outDir + '/BKP'
            unix.mkdir(bkpDir)
            
            # initialize variable 
            clipConsensus = None
            
            # if cluster orientation is plus
            if metacluster.orientation == 'PLUS':
                
                # if there is only a clipping event
                if len(eventsDict['RIGHT-CLIPPING']) == 1:
                    clipConsensus = eventsDict['RIGHT-CLIPPING'][0].clipped_seq()
                
                # if there is more than a clipping event
                elif len(eventsDict['RIGHT-CLIPPING']) > 1:
                    clipConsensusPath, clipConsensus = bkp.makeConsSeqs(eventsDict['RIGHT-CLIPPING'], 'INT', bkpDir)
            
            # if cluster orientation is minus
            elif metacluster.orientation == 'MINUS':
                
                # if there is only a clipping event
                if len(eventsDict['LEFT-CLIPPING']) == 1:
                    clipConsensus = eventsDict['LEFT-CLIPPING'][0].clipped_seq()
                
                # if there is more than a clipping event
                elif len(eventsDict['LEFT-CLIPPING']) > 1:
                    clipConsensusPath, clipConsensus = bkp.makeConsSeqs(eventsDict['LEFT-CLIPPING'], 'INT', bkpDir)
            
            ## 3. polyA search if there is a consensus
            if clipConsensus:
                
                # set metacluster identity to partnered if there is polyA/polyT tail in consensus seq
                if retrotransposons.has_polyA_illumina(clipConsensus): metacluster.identity = 'partnered'
        
        # set metacluster identity to orphan if metacluster not partnered
        if metacluster.identity != 'partnered': metacluster.identity = 'orphan'

def identity_metaclusters_wgs(metaclusters, bam, outDir, confDict, annotations, srcId):
    '''
    Determine metaclusters identity using discordants around the insertion
    
    Input:
    1. metaclusters: list of metaclusters
    2. bam
    3. outDir
    4. confDict: original config dictionary
    5. annotations: nested dictionary of ['REPEATS', 'TRANSDUCTIONS'] (first keys) containing 
                    annotated repeats organized per chromosome (second keys) into genomic bins (values)
    6. srcId: metacluster source id 

    Output: metacluster.identity attribute is filled
    '''
    
    # create newconfDict to collect discordants around insertion
    newconfDict = confDict
    newconfDict['targetEvents'] = ['DISCORDANT', 'CLIPPING']
    newconfDict['minMAPQ'] = 20
    newconfDict['filterDuplicates'] = True
    newconfDict['readFilters'] = ['mateUnmap', 'insertSize', 'SMS']
    
    # for each metacluster
    for metacluster in metaclusters:
        
        # set src id
        metacluster.src_id = srcId
        
        # collect discordants in region
        buffer = confDict['readSize'] * 2
        eventsDict = bamtools.collectSV(metacluster.ref, metacluster.refLeftBkp-buffer, metacluster.refRightBkp+buffer, bam, newconfDict, None, supplementary = False)
        
        # determine identity if there is discordants
        if 'DISCORDANT' in eventsDict.keys():
            
            ## 1. Transform clippings in discordants
            clippings = []

            if 'LEFT-CLIPPING' in eventsDict.keys():
                clippings += eventsDict['LEFT-CLIPPING']
            
            if 'RIGHT-CLIPPING' in eventsDict.keys():
                clippings += eventsDict['RIGHT-CLIPPING']
                
            discordants_SA = events.SA_as_DISCORDANTS(clippings, confDict['readSize'])
            discordants = eventsDict['DISCORDANT'] + discordants_SA
            
            ## 2. Determine discordant identity
            discordantEventsIdent = events.determine_discordant_identity_MEIs(discordants, annotations['REPEATS'], annotations['TRANSDUCTIONS'], confDict['readSize'])
            
            # Add pA support 
            discordantsIdentDict = retrotransposons.add_polyA_discordantsIdentDict(discordantEventsIdent)
            
            ## 3. Determine metacluster identity
            retrotransposons.determine_MEI_type_discordants(metacluster, discordantsIdentDict)