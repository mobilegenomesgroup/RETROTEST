## DEPENDENCIES ##
# External
import pybedtools
from collections import Counter

# GAPI
from GAPI import formats


def write_transduction_calls(clustersPerSrc, outDir, germlineDb):
    '''
    Compute transduction counts per source element and write into file

    Input:
        1. clustersPerSrc: Dictionary containing one key per source element and the list of identified transduction clusters 
        2. outDir: Output directory
        3. germlineDb: germline MEI bed file 
        
    Output: tsv file containing transduction counts per source element ordered by chromosome and then by start position
    '''
    ## 1. Write header 
    outFilePath = outDir + '/transduction_calls.tsv'
    outFile = open(outFilePath, 'w')
    row = "#ref \t beg \t end \t srcId \t tdType \t orientation \t nbReads \t nbDiscordant \t nbClipping \t readIds \n"
    outFile.write(row)

    ## 2. Generate list containing transduction calls
    call = None 
    calls = []

    # For each source element
    for srcId, clusters in clustersPerSrc.items(): 

        # For each cluster
        for cluster in clusters:
            
            readIds = ','.join(cluster.supportingReads()[3])
            
            # if bkp has being defined, use it as call coordinates
            beg = cluster.refLeftBkp if cluster.refLeftBkp is not None else cluster.beg
            end = cluster.refRightBkp if cluster.refRightBkp is not None else cluster.end
            
            # gene annotation
            region, gene = cluster.geneAnnot if hasattr(cluster, 'geneAnnot') else ("None", "None")
            
            call = [cluster.ref, str(beg), str(end), srcId, str(cluster.identity), str(cluster.orientation), str(cluster.supportingReads()[0]), str(cluster.nbDISCORDANT()), str(cluster.nbSUPPLEMENTARY()), str(region), str(gene), readIds]
            calls.append(call)

    ## 3. Sort transduction calls first by chromosome and then by start position
    calls.sort(key=lambda x: (x[0], int(x[1])))

    ## 4. Write transduction calls into output file
    for transduction in calls:
        row = "\t".join(transduction) + "\n"
        outFile.write(row)
    
    outFile.close()
    
    # if there is calls
    if call is not None:
    
        outFile = pybedtools.BedTool(outFilePath)

        ## 5. Collapse calls when pointing to the same MEI. It happens when source elements are too close
        colList = list(range(4, len(call)+1))
        colFormat = ['distinct'] * (len(call) - 3)
        outBed = outFile.merge(c=colList, o=colFormat, d=100, header=True)

        ## 6. Filter germline variants if orphan germline DB provided
        if germlineDb:
            
            dbBed = pybedtools.BedTool(germlineDb)
            outBed = outBed.intersect(b=dbBed, v=True, header=True)
        
        outBed.saveas(outFilePath)
    
    ## 7. Write final transduction counts to file
    write_transduction_counts(outFilePath, outDir)


def write_transduction_counts(outBed, outDir):
    '''
    Compute transduction counts per source element and write into file

    Input:
        1. outBed: RetroTest output bed file 
        2. outDir: Output directory
        
    Output: tsv file containing orphan counts per source element
    '''
    ## 1. Open output bed 
    BED = formats.BED()
    BED.read(outBed, 'List', None)
    
    ## 2. Select orphan calls
    src_ids = [line.optional['srcId'] for line in BED.lines if line.optional['tdType'] == "orphan"]
    
    ## 3. Count orphans per source id
    srcCounter = Counter(src_ids)

    ## 4. Write counts to output file
    outFilePath = outDir + '/orphan_counts.tsv'
    outFile = open(outFilePath, 'w')
    row = "#srcId \t nbTransductions \n"
    outFile.write(row)
    
    for srcId, nbTd in srcCounter.items():
        row = "\t".join([srcId, str(nbTd), "\n"])
        outFile.write(row)
            
    outFile.close()
