#!/usr/bin/env python
#coding: utf-8


if __name__ == '__main__':
	
	## Check dependencies, true if there are some missing dependencies ##
	# missing_dependencies=cd.missing_python_dependencies() or cd.missing_program_dependencies()

	####################
	## Import modules ##
	####################

	# External
	import argparse
	import sys
	import os
	import multiprocessing as mp

	# GAPI
	from GAPI import log
	from GAPI import bamtools

	# Iternal
	from modules import caller

	mp.set_start_method('spawn')


	######################
	## Get user's input ##
	######################

	## 1. Define parser ##
	### Mandatory arguments
	parser = argparse.ArgumentParser(description='Call transductions for a set of target loci from transduction capture or whole genome sequencing data. Two running modes: 1) SINGLE: individual sample; 2) PAIRED: tumour and matched normal sample')
	parser.add_argument('bam', help='Input bam file. Will correspond to the tumour sample in the PAIRED mode')
	parser.add_argument('reference', help='Reference genome in fasta format. An index of the reference generated with samtools faidx must be located in the same directory')
	parser.add_argument('refDir', help='Directory containing reference databases (consensus sequences, source elements...)')

	### Optional arguments
	## General
	parser.add_argument('--normalBam', default=None, dest='normalBam', help='Matched normal bam file. If provided MEIGA will run in PAIRED mode')
	parser.add_argument('--retroTestWGS', action="store_true", default=False, dest='retroTestWGS', help='Apply Retrotest method on WGS data. Default: False')
	parser.add_argument('--blatClip', action="store_true", default=False, dest='blatClip', help='When selected, blat realignment will be performed with clippings. Higher sensitivity, but time-consuming. Default: False')	
	parser.add_argument('--transduction-end', default=3, dest='tdEnd', type=int, help='DNA end to look for transductions, 5 or 3 end. By default: 3 end is used')
	parser.add_argument('--source-bed', default=None, dest='srcBed', help='Bed file contianing competent source elements to look for transductions. By default: srcElements.bed will be used')	
	parser.add_argument('--germlineMEI', default=None, dest='germlineMEI', help='Bed file containing set of known germline MEI to be discarded')
	parser.add_argument('--gene-annot-dir', default=None, dest='annovarDir', help='Directory containing annovar reference files for gene-based annotation of MEI breakpoints. If not provided gene annotation step will be skipped')
	parser.add_argument('-p', '--processes', default=1, dest='processes', type=int, help='Number of processes. Default: 1')
	parser.add_argument('-o', '--outDir', default=os.getcwd(), dest='outDir', help='Output directory. Default: current working directory')
	
	## BAM processing
	parser.add_argument('--no-duplicates', action="store_true", default=False, dest='filterDuplicates', help='Filter out reads marked as duplicates if filter enabled')
	parser.add_argument('--readFilters', default=None, dest='readFilters', type=str, help='Comma separated list of possible read filters. Default: None. Options: [mateUnmap, insertSize, SMS, noDuplicates]')
	parser.add_argument('--minMAPQ', default=20, dest='minMAPQ', type=int, help='Minimum mapping quality required for each read. Default: 20')
	parser.add_argument('--minCLIPPINGlen', default=20, dest='minCLIPPINGlen', type=int, help='Minimum clipped sequence length for each read. Default: 20')

	## Clustering
	parser.add_argument('--minClusterSize', default=4, dest='minClusterSize', type=int, help='Minimum number of reads composing a cluster. Default: 4')
	parser.add_argument('--maxClusterSize', default=500, dest='maxClusterSize', type=int, help='Maximum number of reads composing a metacluster. Default: 500')
	parser.add_argument('--BKPdist', default=50, dest='maxBkpDist', type=int, help='Maximum distance bewteen two adjacent breakpoints for CLIPPING clustering (Between 0-999). Default: 250')
	parser.add_argument('--equalOrientBuffer', default=200, dest='equalOrientBuffer', type=int, help='Distance between reads that are equally oriented. Default: 200')
	parser.add_argument('--oppositeOrientBuffer', default=600, dest='oppositeOrientBuffer', type=int, help='Distance between reads that are opposite oriented. Default: 600')

	## Filtering
	parser.add_argument('--minReads', default=4, dest='minReads', type=int, help='Minimum number of reads supporting a SV. Default: 4')
	parser.add_argument('--minNormalReads', default=2, dest='minNormalReads', type=int, help='Minimum number of reads supporting a SV in normal sample. Default: 2')

	parser.add_argument('--minReadsRegionMQ', default=1, dest='minReadsRegionMQ', type=int, help='Surrounding reads above this MQ are considered low MQ reads. Default: 10')
	parser.add_argument('--maxRegionlowMQ', default=0.5, dest='maxRegionlowMQ', type=int, help='Maximum percentage of lowMAPQ/nbReads in cluster´s region. Default: 0.3')
	parser.add_argument('--maxRegionSMS', default=0.10, dest='maxRegionSMS', type=int, help='Maximum percentage of SMS clipping reads in cluster´s region. Default: 0.15')

	parser.add_argument('--filtersBfClip', default="MAX-NBREADS,AREAMAPQ,AREASMS,IDENTITY", dest='filtersBfClip', type=str, help='Comma separated list of filters to apply before adding clippings to metaclusters. Default: MAX-NBREADS,AREAMAPQ,AREASMS,IDENTITY')
	parser.add_argument('--filtersAfClip', default="MIN-NBREADS,MAX-NBREADS", dest='filtersAfClip', type=str, help='Comma separated list of filters to apply after adding clippings to metaclusters. Default: MIN-NBREADS, MAX-NBREADS')
	
	## 2. Parse user´s input and initialize variables ##
	args = parser.parse_args()

	### Mandatory arguments
	bam = args.bam
	reference = args.reference
	refDir = args.refDir

	### Optional arguments
	## General
	normalBam = args.normalBam
	retroTestWGS = args.retroTestWGS	
	blatClip = args.blatClip
	tdEnd = args.tdEnd
	srcBed = args.srcBed
	germlineMEI = args.germlineMEI
	annovarDir = args.annovarDir
	processes = args.processes
	outDir = args.outDir

	## BAM processing
	filterDuplicates = args.filterDuplicates
	readFilters = args.readFilters
	minMAPQ = args.minMAPQ
	minCLIPPINGlen = args.minCLIPPINGlen

	## Clustering
	minClusterSize = args.minClusterSize
	maxClusterSize = args.maxClusterSize
	maxBkpDist = args.maxBkpDist
	equalOrientBuffer = args.equalOrientBuffer
	oppositeOrientBuffer = args.oppositeOrientBuffer

	## Filtering thresholds
	minReads = args.minReads
	minNormalReads = args.minNormalReads

	minReadsRegionMQ = args.minReadsRegionMQ
	maxRegionlowMQ = args.maxRegionlowMQ
	maxRegionSMS = args.maxRegionSMS

	filtersBfClip = args.filtersBfClip
	filtersAfClip = args.filtersAfClip
			
    # ## Check file dependencies (list with the paths) ##
	# missing_dependencies = missing_dependencies or  cd.missing_needed_files((bam,reference,refDir,normalBam,annovarDir,outDir))

	# if missing_dependencies:
	# 	exit()

	###########################################
	# Input checks plus input data conversion #
	###########################################

	# Get all that are present in the bam file.
	refs = bamtools.get_refs(bam)

	# Convert comma-separated string inputs into lists:
	targetRefs = refs.split(',')

	# Convert comma-separated string inputs into lists:
	filtersBfClipList = filtersBfClip.split(',')
	filtersAfClipList = filtersAfClip.split(',')

	## Determine running mode:
	mode = 'SINGLE' if normalBam == None else 'PAIRED'

	##############################################
	## Display configuration to standard output ##
	##############################################
	scriptName = os.path.basename(sys.argv[0])
	scriptName = os.path.splitext(scriptName)[0]
	version='1.0.2'

	print()
	print('***** ', scriptName, version, 'configuration *****')
	print('*** Mandatory arguments ***')
	print('bam: ', bam)
	print('reference: ', reference)
	print('refDir: ', refDir, "\n")

	print('*** Optional arguments ***')
	print('** General **')
	print('normalBam: ', normalBam)
	print('retroTestWGS: ', retroTestWGS)
	print('blatClip: ', blatClip)
	print('tdEnd: ', tdEnd)
	print('srcBed: ', srcBed)
	print('germlineMEI: ', germlineMEI)
	print('gene-annot-dir: ', annovarDir)
	print('processes: ', processes)
	print('outDir: ', outDir, "\n") 

	print('** BAM processing **')
	print('filterDuplicates: ', filterDuplicates)
	print('readFilters: ', readFilters)
	print('minMAPQ: ', minMAPQ)
	print('minCLIPPINGlength: ', minCLIPPINGlen, "\n")

	print('** Clustering **')
	print('minClusterSize: ', minClusterSize)
	print('maxClusterSize: ', maxClusterSize)
	print('maxBkpDist: ', maxBkpDist)
	print('equalOrientBuffer: ', equalOrientBuffer)
	print('oppositeOrientBuffer: ', oppositeOrientBuffer, "\n")

	print('** Filtering **')
	print('minReads: ', minReads)
	print('minNormalReads: ', minNormalReads)
	print('minReadsRegionMQ: ', minReadsRegionMQ)
	print('maxRegionlowMQ: ', maxRegionlowMQ)
	print('maxRegionSMS: ', maxRegionSMS)
	print('filtersBfClip:', filtersBfClip)
	print('filtersAfClip:', filtersAfClip, "\n")
	
	print('***** Executing ', scriptName, '.... *****', "\n")

	##########
	## CORE ## 
	##########

	## 1. Create configuration dictionary
	#######################################
	confDict = {}

	## General
	confDict['retroTestWGS'] = retroTestWGS
	confDict['blatClip'] = blatClip
	confDict['srcBed'] = srcBed
	confDict['tdEnd'] = tdEnd
	confDict['germlineMEI'] = germlineMEI
	confDict['annovarDir'] = annovarDir
	confDict['processes'] = processes

	## BAM processing
	confDict['filterDuplicates'] = filterDuplicates	
	confDict['readFilters'] = readFilters
	confDict['minMAPQ'] = minMAPQ
	confDict['minCLIPPINGlen'] = minCLIPPINGlen
	confDict['targetRefs'] = targetRefs

	if confDict['readFilters'] == None:
		confDict['readFilters'] = ['mateUnmap', 'insertSize', 'SMS']

	## Target SV events to search for
	confDict['targetEvents'] = ['DISCORDANT', 'CLIPPING']

	## Clustering
	confDict['minClusterSize'] = minClusterSize
	confDict['maxBkpDist'] = maxBkpDist
	confDict['equalOrientBuffer'] = equalOrientBuffer
	confDict['oppositeOrientBuffer'] = oppositeOrientBuffer
			
	## Filtering thresholds
	confDict['minReads'] = minReads
	confDict['minNormalReads'] = minNormalReads
	
	confDict['minNbDISCORDANT'] = minClusterSize
	confDict['minNbCLIPPING'] = minClusterSize
	confDict['maxNbDISCORDANT'] = maxClusterSize
	confDict['maxNbCLIPPING'] = maxClusterSize

	confDict['minReadsRegionMQ'] = minReadsRegionMQ
	confDict['maxRegionlowMQ'] = maxRegionlowMQ
	confDict['maxRegionSMS'] = maxRegionSMS
	
	confDict['filtersBfClip'] = filtersBfClipList
	confDict['filtersAfClip'] = filtersAfClipList
 	
	## 2. Execute structural variation caller
	###########################################
	tdCaller = caller.transduction_caller(mode, bam, normalBam, reference, refDir, confDict, outDir)

	### Do calling
	tdCaller.call()

	print('***** Finished! *****')
	print()
